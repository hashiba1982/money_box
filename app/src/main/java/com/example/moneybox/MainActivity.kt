package com.example.moneybox

import android.annotation.SuppressLint
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Bundle
import android.os.IBinder
import android.os.RemoteException
import android.print.PrintAttributes
import android.print.PrintJob
import android.print.PrintManager
import android.util.Log
import android.view.KeyEvent
import android.webkit.JavascriptInterface
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.annotation.Nullable
import androidx.appcompat.app.AlertDialog
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.example.moneybox.response.*
import com.google.gson.Gson
import com.google.zxing.BarcodeFormat
import com.google.zxing.EncodeHintType
import com.google.zxing.MultiFormatWriter
import com.journeyapps.barcodescanner.BarcodeEncoder
import com.sunmi.peripheral.printer.InnerPrinterCallback
import com.sunmi.peripheral.printer.InnerPrinterException
import com.sunmi.peripheral.printer.InnerPrinterManager
import com.sunmi.peripheral.printer.SunmiPrinterService
import kotlinx.android.synthetic.main.activity_main.*
import woyou.aidlservice.jiuiv5.ICallback
import woyou.aidlservice.jiuiv5.IWoyouService
import java.util.*


class MainActivity : BaseScreenShotActivity() {

    private var iWoyouService: IWoyouService? = null
    private var sunmiPrinterService: SunmiPrinterService? = null //商米标准打印 打印服务
    private var connected = false
    private var screenManager = ScreenManager.getInstance()
    private var smallDisplay: SmallDisplay? = null
    private var isShowSmallDisplay = false
    private var printView:WebView? = null

    private var isExit: Boolean? = false
    private var hasTask: Boolean? = false
    val timerExit = Timer()
    val task: TimerTask = object : TimerTask() {
        override fun run() {
            isExit = false
            hasTask = true
        }
    }

    private val serviceConnection: ServiceConnection = object : ServiceConnection {
        override fun onServiceConnected(name: ComponentName, service: IBinder) {
            Log.w("John", "onServiceConnected on")
            iWoyouService = IWoyouService.Stub.asInterface(service)
            connected = true
        }

        override fun onServiceDisconnected(name: ComponentName) {
            Log.w("John", "onServiceConnected off")
            iWoyouService = null
            connected = false
        }
    }

    private fun bindService() {
        val intent = Intent()
        intent.setPackage("woyou.aidlservice.jiuiv5")
        intent.action = "woyou.aidlservice.jiuiv5.IWoyouService"
        startService(intent)
        bindService(intent, serviceConnection, BIND_AUTO_CREATE)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

/*        SunmiPrintHelper.getInstance().initSunmiPrinterService(this)
        if (sunmiPrinterService == null){
            sunmiPrinterService = SunmiPrintHelper.getInstance().sunmiPrinterService
        }*/

        bindService()
        initWebView()
        initSecondScreen()
        //connectPrintService()
    }

    private fun initSecondScreen(url:String? = "") {

        if (!isShowSmallDisplay){
            screenManager.init(this)

            var display = screenManager.presentationDisplays

            if (display != null) {
                smallDisplay = SmallDisplay(this, display)
                smallDisplay?.show()
                isShowSmallDisplay = true
            }
        }else{
            smallDisplay?.changeUrl(url!!)
        }

    }

    private fun initWebView(){
        //var url = "http://gu.ifortunetech.tw/apk.html"
        var url = "http://phonecase.ifortunetech.tw/store/store/loginGateway"
        //var url = "file:///android_asset/test.html"
        //var url = "http://phonecase.ifortunetech.tw/store/pagePrint/demo"

        web_big.settings.javaScriptEnabled = true
        web_big.settings.loadWithOverviewMode = true
        web_big.settings.useWideViewPort = true
        web_big.settings.defaultTextEncodingName = "utf-8"
        web_big.webViewClient = object : WebViewClient(){
            override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
                //return super.shouldOverrideUrlLoading(view, request)
                return false
            }

            override fun onPageFinished(view: WebView?, url: String?) {
                super.onPageFinished(view, url)
                printView = view
            }
        }

        web_big.addJavascriptInterface(object : Any() {
            @JavascriptInterface // For API 17+
            @SuppressLint("JavascriptInterface")
            fun performClick() {
                try {
                    //打開錢箱
  /*                  iWoyouService?.openDrawer(object : ICallback.Stub() {
                        override fun onRunResult(isSuccess: Boolean, code: Int, msg: String?) {
                            Log.w("John", "onRunResult:${isSuccess}==${code}==${msg}")
                        }
                    })*/
                    if (sunmiPrinterService == null){
                        sunmiPrinterService = SunmiPrintHelper.getInstance().sunmiPrinterService
                    }
                    sunmiPrinterService?.openDrawer(null)
                } catch (e: RemoteException) {
                    e.printStackTrace()
                }
            }

            @JavascriptInterface // For API 17+
            @SuppressLint("JavascriptInterface")
            fun performPrint(response:String){
                try {

                    if (sunmiPrinterService == null){
                        sunmiPrinterService = SunmiPrintHelper.getInstance().sunmiPrinterService
                    }

                    val mResponse = Gson().fromJson(response, BaseResponse::class.java)

                    when(mResponse.type){
                        1 -> {
                            val type1Result = Gson().fromJson(response, BaseResponse1::class.java)
                            type1PrintReceipt(type1Result.data)
                        }
                        2 -> {
                            val type2Result = Gson().fromJson(response, BaseResponse2::class.java)
                            if (type2Result.data.companyMark.type == "image"){
                                Glide.with(this@MainActivity).asBitmap()
                                    .load(type2Result.data.companyMark.value)
                                    .into(object : CustomTarget<Bitmap?>() {
                                        override fun onResourceReady(resource: Bitmap, @Nullable transition: Transition<in Bitmap?>?) {
                                            type2PrintReceipt(type2Result.data, resource)
                                        }

                                        override fun onLoadCleared(@Nullable placeholder: Drawable?) {}
                                    })
                            }else{
                                type2PrintReceipt(type2Result.data)
                            }
                        }
                        3 -> {
                            val type3Result = Gson().fromJson(response, BaseResponse3::class.java)
                            type3PrintReceipt(type3Result.data, 3)
                        }
                        4 -> {
                            val type3Result = Gson().fromJson(response, BaseResponse3::class.java)
                            type3PrintReceipt(type3Result.data, 4)
                        }
                        5 -> {
                            val type5Result = Gson().fromJson(response, BaseResponse2::class.java)
                            if (type5Result.data.companyMark.type == "image"){
                                Glide.with(this@MainActivity).asBitmap()
                                    .load(type5Result.data.companyMark.value)
                                    .into(object : CustomTarget<Bitmap?>() {
                                        override fun onResourceReady(resource: Bitmap, @Nullable transition: Transition<in Bitmap?>?) {
                                            type5PrintReceipt(type5Result.data, resource)
                                        }

                                        override fun onLoadCleared(@Nullable placeholder: Drawable?) {}
                                    })
                            }else{
                                type5PrintReceipt(type5Result.data)
                            }
                        }
                    }
                } catch (e: RemoteException){
                    e.printStackTrace()
                }
            }

            @JavascriptInterface // For API 17+
            @SuppressLint("JavascriptInterface")
            fun switchView(url:String) {
                try {
                    runOnUiThread {
                        smallDisplay?.changeUrl(url)
                    }
                } catch (e: RemoteException) {
                    e.printStackTrace()
                }
            }

            @JavascriptInterface // For API 17+
            @SuppressLint("JavascriptInterface")
            fun printWebView() {
                printView?.let {
                    createWebPrintJob(it)
                }
            }

            @JavascriptInterface // For API 17+
            @SuppressLint("JavascriptInterface")
            fun printExtra(url:String) {
                var intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
                startActivity(intent)
            }
        }, "ok")
        web_big.loadData("", "text/html", null)
        web_big.loadUrl(url)
    }

    private fun type1PrintReceipt(data:Type1ReceiptResponse){
        try {
            sunmiPrinterService?.setAlignment(0, null)
            sunmiPrinterService?.setFontSize(24f, null)

            for (i in data.indices){
                sunmiPrinterService?.printColumnsString(arrayOf(data[i].item, "$"+data[i].value), intArrayOf(1,1), intArrayOf(0,2),null)
            }

            //結束
            sunmiPrinterService?.lineWrap(4, null)
            sunmiPrinterService?.cutPaper(null)
        }catch (e:Exception){
            e.printStackTrace()
        }
    }

    private fun type5PrintReceipt(data:Type2ReceiptResponse, logoBmp:Bitmap? = null){
        //woyouService?.lineWrap(2, null)
        sunmiPrinterService?.setAlignment(1, null)
        if (logoBmp != null){
            sunmiPrinterService?.printBitmap(logoBmp, null)
        }else{
            sunmiPrinterService?.printTextWithFont(data.companyMark.value+"\n", "", 40f, null)
        }
        sunmiPrinterService?.printTextWithFont(data.invoiceTitle+"\n", "", 48f, null)
        sunmiPrinterService?.sendRAWData(byteArrayOf(0x1B, 0x45, 0x1), null)
        sunmiPrinterService?.printTextWithFont(data.period+"\n", "", 48f, null)
        sunmiPrinterService?.printTextWithFont(data.invoiceNo+"\n", "", 48f, null)
        sunmiPrinterService?.sendRAWData(byteArrayOf(0x1B, 0x45, 0x0), null)

        sunmiPrinterService?.setAlignment(0, null)
        sunmiPrinterService?.printTextWithFont(data.createdAt+"\n", "", 24f, null)
        sunmiPrinterService?.setFontSize(24f, null)
        sunmiPrinterService?.printColumnsString(arrayOf("隨機碼:" + data.random, "總計:" + data.totalAmount), intArrayOf(1,1), intArrayOf(0,2),null)
        sunmiPrinterService?.printTextWithFont("賣方:"+data.salerGuiNumber+"\n", "", 24f, null)
        sunmiPrinterService?.printText("\n", null)
        //woyouService?.printBarCode(data.barcode, 4, 50, 6, 2, null)
        sunmiPrinterService?.setAlignment(1, null)

        try {
            val barcodeEncoder = BarcodeEncoder()
            val writer = MultiFormatWriter()
            val hints: Hashtable<EncodeHintType, Any> = Hashtable()
            hints[EncodeHintType.CHARACTER_SET] = "utf-8"

            var barcodeEncode = writer.encode(data.barcode, BarcodeFormat.CODE_39, 200, 50, hints)
            val barcodeBmp = barcodeEncoder.createBitmap(barcodeEncode)
            sunmiPrinterService?.printBitmap(barcodeBmp, null)

            var qrcode1 = writer.encode(data.qrcode1, BarcodeFormat.QR_CODE, 200, 200, hints)
            var qrcode2 = writer.encode(data.qrcode2, BarcodeFormat.QR_CODE, 200, 200, hints)
            val bitmapLeft = barcodeEncoder.createBitmap(qrcode1)
            val bitmapRight = barcodeEncoder.createBitmap(qrcode2)

            var qrCodeBitmap = BitmapTools.mergeBitmap_LR(bitmapLeft, bitmapRight, true)
            sunmiPrinterService?.printBitmap(qrCodeBitmap, null)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        sunmiPrinterService?.printText("\n", null)

        //結束
        sunmiPrinterService?.lineWrap(4, null)
        sunmiPrinterService?.cutPaper(null)
    }

    private fun type2PrintReceipt(data:Type2ReceiptResponse, logoBmp:Bitmap? = null){
        //woyouService?.lineWrap(2, null)
        sunmiPrinterService?.setAlignment(1, null)
        if (logoBmp != null){
            sunmiPrinterService?.printBitmap(logoBmp, null)
        }else{
            sunmiPrinterService?.printTextWithFont(data.companyMark.value+"\n", "", 40f, null)
        }
        sunmiPrinterService?.printTextWithFont(data.invoiceTitle+"\n", "", 48f, null)
        sunmiPrinterService?.sendRAWData(byteArrayOf(0x1B, 0x45, 0x1), null)
        sunmiPrinterService?.printTextWithFont(data.period+"\n", "", 48f, null)
        sunmiPrinterService?.printTextWithFont(data.invoiceNo+"\n", "", 48f, null)
        sunmiPrinterService?.printText("\n", null)
        sunmiPrinterService?.sendRAWData(byteArrayOf(0x1B, 0x45, 0x0), null)

        sunmiPrinterService?.setAlignment(0, null)
        sunmiPrinterService?.setFontSize(24f, null)
        sunmiPrinterService?.printColumnsString(arrayOf(data.createdAt, "格式 25"), intArrayOf(2,1), intArrayOf(0,2),null)
        sunmiPrinterService?.printColumnsString(arrayOf("隨機碼:" + data.random, "總計:" + data.totalAmount), intArrayOf(1,1), intArrayOf(0,2),null)
        sunmiPrinterService?.printColumnsString(arrayOf("賣方:"+data.salerGuiNumber, "買方:"+data.buyerGuiNumber), intArrayOf(1,1), intArrayOf(0,2),null)
        sunmiPrinterService?.printText("\n", null)
        //woyouService?.printBarCode(data.barcode, 8, 50, 6, 2, null);
        sunmiPrinterService?.setAlignment(1, null)

        try {
            val barcodeEncoder = BarcodeEncoder()
            val writer = MultiFormatWriter()
            val hints: Hashtable<EncodeHintType, Any> = Hashtable()
            hints[EncodeHintType.CHARACTER_SET] = "utf-8"

            var barcodeEncode = writer.encode(data.barcode, BarcodeFormat.CODE_39, 200, 50, hints)
            val barcodeBmp = barcodeEncoder.createBitmap(barcodeEncode)
            sunmiPrinterService?.printBitmap(barcodeBmp, null)

            var qrcode1 = writer.encode(data.qrcode1, BarcodeFormat.QR_CODE, 200, 200, hints)
            var qrcode2 = writer.encode(data.qrcode2, BarcodeFormat.QR_CODE, 200, 200, hints)
            val bitmapLeft = barcodeEncoder.createBitmap(qrcode1)
            val bitmapRight = barcodeEncoder.createBitmap(qrcode2)

            var qrCodeBitmap = BitmapTools.mergeBitmap_LR(bitmapLeft, bitmapRight, true)
            sunmiPrinterService?.printBitmap(qrCodeBitmap, null)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        sunmiPrinterService?.printText("\n", null)

        //以下是交易明細
        sunmiPrinterService?.setAlignment(1, null)
        sunmiPrinterService?.printTextWithFont("------------------------" + "\n\n", "", 30f, null)
        sunmiPrinterService?.printTextWithFont("交易明細" + "\n\n", "", 34f, null)

        sunmiPrinterService?.setAlignment(0, null)
        sunmiPrinterService?.setFontSize(24f, null)
        sunmiPrinterService?.printTextWithFont("買受人統編:"+data.buyerGuiNumber+"\n", "", 24f, null)
        sunmiPrinterService?.printTextWithFont("買受人名稱:"+data.buyerGuiNumber+"\n", "", 24f, null)
        sunmiPrinterService?.printTextWithFont(data.createdAt+"\n", "", 24f, null)
        sunmiPrinterService?.printText("\n", null)

        sunmiPrinterService?.setAlignment(1, null)
        sunmiPrinterService?.printColumnsString(arrayOf("品名", "單價*數量", "金額"), intArrayOf(1,1,1), intArrayOf(0,1,2),null)
        for (i in data.detail.indices){
            val count = data.detail[i].price + " X"+data.detail[i].quantity
            val amount = "$ "+data.detail[i].amount + data.detail[i].taxType
            sunmiPrinterService?.printColumnsString(arrayOf(data.detail[i].item), intArrayOf(1), intArrayOf(0),null)
            sunmiPrinterService?.printColumnsString(arrayOf(count), intArrayOf(1), intArrayOf(1),null)
            sunmiPrinterService?.printColumnsString(arrayOf(amount), intArrayOf(1), intArrayOf(2),null)
        }
        sunmiPrinterService?.printTextWithFont("------------------------" + "\n", "", 30f, null)

        sunmiPrinterService?.setAlignment(0, null)
        sunmiPrinterService?.setFontSize(24f, null)
        sunmiPrinterService?.printTextWithFont("合計" + data.detail.size+"項\n", "", 24f, null)
        val noTaxAmount = data.totalAmount.toInt()-data.taxAmount.toInt()
        sunmiPrinterService?.printTextWithFont("銷售額(應稅): $" + noTaxAmount + "\n", "", 24f, null)
        sunmiPrinterService?.printTextWithFont("稅額: $" + data.taxAmount + "\n", "", 24f, null)
        sunmiPrinterService?.printTextWithFont("總計: $" + data.totalAmount, "", 24f, null)

        //QRCODE
        //woyouService?.lineWrap(1, null)
        //woyouService?.printTextWithFont("------------------------" + "\n\n", "", 30f, null)
        //woyouService?.lineWrap(1, null)
        //woyouService?.printQRCode("https://sunmi.com/", 6, 30, null)  //另一個sample是8,0
        //woyouService?.lineWrap(1, null)
        //woyouService?.setFontSize(0f, null)
        //woyouService?.printText("THANK", null)

        //結束
        sunmiPrinterService?.lineWrap(4, null)
        sunmiPrinterService?.cutPaper(null)
    }

    private fun type3PrintReceipt(data:Type3ReceiptResponse, mode:Int){
        sunmiPrinterService?.setAlignment(1, null)
        sunmiPrinterService?.setFontSize(28f, null)

        data.forEach {
            if (it.type == "info"){
                sunmiPrinterService?.printColumnsString(arrayOf(it.title, it.value), intArrayOf(1,1), intArrayOf(0,2),null)
            }
        }

        sunmiPrinterService?.printColumnsString(arrayOf("", "交易明細", ""), intArrayOf(1,3,1), intArrayOf(0,1,2),null)

        if (mode == 3){
            sunmiPrinterService?.printColumnsString(arrayOf("品名", "單價*數量", "金額"), intArrayOf(1,1,1), intArrayOf(0,1,2),null)
            data.forEach {
                if (it.type == "detail"){
                    it.content.forEach { detail ->
                        sunmiPrinterService?.sendRAWData(byteArrayOf(0x1B, 0x45, 0x1), null)
                        sunmiPrinterService?.printColumnsString(arrayOf(detail.title), intArrayOf(1), intArrayOf(0),null)
                        sunmiPrinterService?.sendRAWData(byteArrayOf(0x1B, 0x45, 0x0), null)
                        val totalPrice = detail.quantity.toInt() * detail.price.toInt()
                        val quantity = detail.price + "*" + detail.quantity
                        sunmiPrinterService?.printColumnsString(arrayOf("", quantity, totalPrice.toString()), intArrayOf(1,1,1), intArrayOf(0,1,2),null)
                    }
                }
            }

        }else {
            sunmiPrinterService?.printColumnsString(arrayOf("品名", "數量"), intArrayOf(1,1), intArrayOf(0,2),null)
            data.forEach {
                if (it.type == "detail"){
                    it.content.forEach { detail ->
                        sunmiPrinterService?.sendRAWData(byteArrayOf(0x1B, 0x45, 0x1), null)
                        sunmiPrinterService?.printColumnsString(arrayOf(detail.title), intArrayOf(1), intArrayOf(0),null)
                        sunmiPrinterService?.sendRAWData(byteArrayOf(0x1B, 0x45, 0x0), null)
                        var quantily = "*"+detail.quantity
                        sunmiPrinterService?.printColumnsString(arrayOf(quantily), intArrayOf(1), intArrayOf(2),null)
                    }
                }
            }

        }

        //結束
        sunmiPrinterService?.lineWrap(4, null)
        sunmiPrinterService?.cutPaper(null)
    }

    private fun createWebPrintJob(webView: WebView) {

        // Get a PrintManager instance
        val printManager = this@MainActivity.getSystemService(Context.PRINT_SERVICE) as PrintManager
        val jobName = getString(R.string.app_name) + " Document"

        // Get a print adapter instance
        val printAdapter = webView.createPrintDocumentAdapter(jobName)

        // Create a print job with name and adapter instance
        val printJob: PrintJob = printManager.print(
            jobName, printAdapter,
            PrintAttributes.Builder().build()
        )
    }

    //连接打印服务
    private fun connectPrintService() {
        try {
            InnerPrinterManager.getInstance().bindService(this, innerPrinterCallback)
        } catch (e: InnerPrinterException) {
            e.printStackTrace()
        }
    }

    private val innerPrinterCallback: InnerPrinterCallback = object : InnerPrinterCallback() {
        override fun onConnected(service: SunmiPrinterService) {
            sunmiPrinterService = service
            sunmiPrinterService?.let {
                checkSunmiPrinterService(it)
            }
        }

        override fun onDisconnected() {
            sunmiPrinterService = null
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (connected) {
            unbindService(serviceConnection)
        }

        if (sunmiPrinterService != null) {
            try {
                InnerPrinterManager.getInstance().unBindService(
                    this,
                    innerPrinterCallback
                )
            } catch (e: InnerPrinterException) {
                e.printStackTrace()
            }
        }
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            // 是否要退出
            if (isExit == false) {

                isExit = true //記錄下一次要退出
                //Toast.makeText(this, , Toast.LENGTH_SHORT).show()
                AlertDialog.Builder(this)
                    .setMessage(getString(R.string.mb_text_01))
                    .setPositiveButton(getString(R.string.mb_text_02)) { dialogInterface, i ->
                        dialogInterface.dismiss()
                        finishAffinity()
                        System.exit(0)
                    }
                    .setNegativeButton(getString(R.string.mb_text_03), { dialogInterface, i ->
                        dialogInterface.dismiss()
                    })
                    .setCancelable(false)
                    .show()


                // 如果超過兩秒則恢復預設值
                if (hasTask == false) {
                    timerExit.schedule(task, 2000)
                }
            } else {
                finishAffinity()
                System.exit(0)
            }
        }
        return false
    }

    private fun checkSunmiPrinterService(service: SunmiPrinterService) {
        var ret = false
        try {
            ret = InnerPrinterManager.getInstance().hasPrinter(service)
        } catch (e: InnerPrinterException) {
            e.printStackTrace()
        }
    }


}