package com.example.moneybox

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.os.RemoteException
import android.view.Display
import android.view.View
import android.webkit.JavascriptInterface
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.annotation.UiThread
import com.example.moneybox.response.BaseResponse
import com.example.moneybox.tools.debug
import com.google.gson.Gson
import kotlinx.android.synthetic.main.small_display_layout.*
import woyou.aidlservice.jiuiv5.ICallback

class SmallDisplay(outerContext: Context?, display: Display?) : BasePresentation(outerContext, display) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.small_display_layout)

        initView()
    }

    private fun initView(){
        //var url = "http://gu.ifortunetech.tw/apk.html"
        var url = "http://phonecase.ifortunetech.tw/"
        //var url = "file:///android_asset/test.html"

        web_small.settings.javaScriptEnabled = true
        web_small.settings.loadWithOverviewMode = true
        web_small.settings.useWideViewPort = true
        web_small.settings.defaultTextEncodingName = "utf-8"
        web_small.webViewClient = object : WebViewClient(){
            override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
                return super.shouldOverrideUrlLoading(view, request)
            }

            override fun onPageFinished(view: WebView?, url: String?) {
                super.onPageFinished(view, url)
            }
        }

        web_small.loadData("", "text/html", null)
        web_small.loadUrl(url)
    }

    fun changeUrl(url:String){
        web_small.clearCache(true)
        web_small.loadData("", "text/html", null)
        web_small.loadUrl(url)
    }

    override fun onClick(v: View) {
        super.onClick(v)
    }

    override fun onSelect(isShow: Boolean) {}
}