package com.example.moneybox.response
import com.google.gson.annotations.SerializedName


class Type1ReceiptResponse : ArrayList<Type1ReceiptResponseItem>()

data class Type1ReceiptResponseItem(
    @SerializedName("item")
    val item: String = "",
    @SerializedName("value")
    val value: String = ""
)