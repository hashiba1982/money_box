package com.example.moneybox.response

import com.google.gson.annotations.SerializedName

class Type3ReceiptResponse : ArrayList<Type3ReceiptResponseItem>()

data class Type3ReceiptResponseItem(
    @SerializedName("content")
    val content: List<Content> = listOf(),
    @SerializedName("style")
    val style: String = "",
    @SerializedName("title")
    val title: String = "",
    @SerializedName("type")
    val type: String = "",
    @SerializedName("value")
    val value: String = ""
)

data class Content(
    @SerializedName("price")
    val price: String = "",
    @SerializedName("quantity")
    val quantity: String = "",
    @SerializedName("title")
    val title: String = ""
)