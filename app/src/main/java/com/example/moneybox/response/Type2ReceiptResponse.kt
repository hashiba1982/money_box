package com.example.moneybox.response

import com.google.gson.annotations.SerializedName


data class Type2ReceiptResponse(
    @SerializedName("barcode")
    val barcode: String = "",
    @SerializedName("buyerGuiNumber")
    val buyerGuiNumber: String = "",
    @SerializedName("companyMark")
    val companyMark: CompanyMark = CompanyMark(),
    @SerializedName("createdAt")
    val createdAt: String = "",
    @SerializedName("detail")
    val detail: List<Detail> = listOf(),
    @SerializedName("invoiceTitle")
    val invoiceTitle: String = "",
    @SerializedName("invoiceNo")
    val invoiceNo: String = "",
    @SerializedName("period")
    val period: String = "",
    @SerializedName("qrcode1")
    val qrcode1: String = "",
    @SerializedName("qrcode2")
    val qrcode2: String = "",
    @SerializedName("random")
    val random: String = "",
    @SerializedName("saleAmount")
    val saleAmount: String = "",
    @SerializedName("salerGuiNumber")
    val salerGuiNumber: String = "",
    @SerializedName("taxAmount")
    val taxAmount: String = "",
    @SerializedName("taxFreeSaleAmount")
    val taxFreeSaleAmount: String = "",
    @SerializedName("taxZeroSaleAmount")
    val taxZeroSaleAmount: String = "",
    @SerializedName("totalAmount")
    val totalAmount: String = ""
)

data class CompanyMark(
    @SerializedName("type")
    val type: String = "",
    @SerializedName("value")
    val value: String = ""
)

data class Detail(
    @SerializedName("amount")
    val amount: String = "",
    @SerializedName("item")
    val item: String = "",
    @SerializedName("price")
    val price: String = "",
    @SerializedName("quantity")
    val quantity: String = "",
    @SerializedName("taxType")
    val taxType: String = ""
)