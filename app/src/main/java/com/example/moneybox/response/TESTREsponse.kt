package com.example.moneybox.response
import com.google.gson.annotations.SerializedName


data class TESTREsponse(
    @SerializedName("data")
    val data: List<Data> = listOf(),
    @SerializedName("type")
    val type: Int = 0
)

data class Data(
    @SerializedName("item")
    val item: String = "",
    @SerializedName("value")
    val value: Any? = Any()
)