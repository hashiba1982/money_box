package com.example.moneybox.response

data class BaseResponse<out T>(val type: Int,  val data: T?)
