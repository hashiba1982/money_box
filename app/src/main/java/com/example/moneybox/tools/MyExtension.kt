package com.example.moneybox.tools

import android.content.Context
import android.content.res.Resources
import android.graphics.drawable.Drawable
import com.google.android.material.snackbar.Snackbar
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.appcompat.app.AppCompatActivity
import android.util.Log
import android.util.TypedValue
import android.util.TypedValue.applyDimension
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.fragment.app.FragmentActivity
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.text.DecimalFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*


open class MyExtension()

fun debug(text: Any) {
    text?.let {
        Log.w("SDOut", text.toString())
    }
}

fun Context?.toast(text: CharSequence, duration: Int = Toast.LENGTH_SHORT) = this?.let { Toast.makeText(it, text, duration).show() }

fun Context?.toast(text: Int, duration: Int = Toast.LENGTH_SHORT) = this?.let { Toast.makeText(it, text, duration).show() }

fun View.showKeyboard() {
    val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    this.requestFocus()
    imm.showSoftInput(this, 0)
}

fun View.hideKeyboard(): Boolean {
    try {
        val inputMethodManager = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        return inputMethodManager.hideSoftInputFromWindow(windowToken, 0)
    } catch (ignored: RuntimeException) {
    }
    return false
}

fun View.showSnackbar(snackbarText: String, timeLength: Int) {
    Snackbar.make(this, snackbarText, timeLength).show()
}

//Fragment調用
inline fun FragmentManager.inTransaction(func: FragmentTransaction.() -> FragmentTransaction) {
    val fragmentTransaction = beginTransaction()
    fragmentTransaction.func()
    fragmentTransaction.commit()
    //beginTransaction().func().commit()
}

//不重複加載Fragment
fun AppCompatActivity.addFragment(fragment: Fragment, frameId: Int, fragmentList: ArrayList<Fragment>) {

    supportFragmentManager.inTransaction {

        fragmentList.forEach {
            if (it != fragment && it.isAdded) hide(it)
        }

        if (fragment.isAdded) {
            show(fragment)
        } else {
            add(frameId, fragment)
        }
    }
}


fun AppCompatActivity.replaceFragment(fragment: Fragment, frameId: Int) {

    supportFragmentManager.inTransaction {
        replace(frameId, fragment)
    }
}


//取得timestamp
fun getTimestamp(): String {
    val tsLong = System.currentTimeMillis() / 1000
    //val calendar = Calendar.getInstance()
    //var tsLong = calendar.timeInMillis

    return tsLong.toString()
}

//前面補零
fun insertZero(num: Int = 0): String {

    var newNum = String.format("%02d", num)
    return newNum
}

//數字格式Format
fun formatNum(num: Any?): String {
    return DecimalFormat("#,###.##").format(num)
}

//dp sp 轉換
val Float.dp get() = applyDimension(TypedValue.COMPLEX_UNIT_DIP, this, Resources.getSystem().displayMetrics)
val Float.sp get() = applyDimension(TypedValue.COMPLEX_UNIT_SP, this, Resources.getSystem().displayMetrics)
val Int.dp get() = toFloat().dp.toInt()
val Int.sp get() = toFloat().sp.toInt()

/** 获取设备显示器的宽度，单位为像素  */
fun AppCompatActivity.getDisplayWidth(): Int {
    //1,获取设备屏幕的分辨率
    //#1,获取windowManager服务
    val windowManager = getSystemService(Context.WINDOW_SERVICE) as WindowManager
    //#2,获取屏幕显示类对象，Display：提供设备屏幕大小密度方向等有关的信息
    val display = windowManager.defaultDisplay
    //#3,获取屏幕的宽度和高度
    return display.width
}

fun FragmentActivity.getDisplayWidth(): Int {
    //1,获取设备屏幕的分辨率
    //#1,获取windowManager服务
    val windowManager = getSystemService(Context.WINDOW_SERVICE) as WindowManager
    //#2,获取屏幕显示类对象，Display：提供设备屏幕大小密度方向等有关的信息
    val display = windowManager.defaultDisplay
    //#3,获取屏幕的宽度和高度
    return display.width
}

/** 获取设备显示器的高度，单位为像素  */
fun AppCompatActivity.getDisplayHeight(): Int {
    //1,获取设备屏幕的分辨率
    //#1,获取windowManager服务
    val windowManager = getSystemService(Context.WINDOW_SERVICE) as WindowManager
    //#2,获取屏幕显示类对象，Display：提供设备屏幕大小密度方向等有关的信息
    val display = windowManager.defaultDisplay
    //#3,获取屏幕的宽度和高度
    return display.height
}

fun FragmentActivity.getDisplayHeight(): Int {
    //1,获取设备屏幕的分辨率
    //#1,获取windowManager服务
    val windowManager = getSystemService(Context.WINDOW_SERVICE) as WindowManager
    //#2,获取屏幕显示类对象，Display：提供设备屏幕大小密度方向等有关的信息
    val display = windowManager.defaultDisplay
    //#3,获取屏幕的宽度和高度
    return display.height
}

//MD5加密
fun encodeMD5(text: String): String {
    try {
        //获取md5加密对象
        val instance: MessageDigest = MessageDigest.getInstance("MD5")
        //对字符串加密，返回字节数组
        val digest: ByteArray = instance.digest(text.toByteArray())
        var sb: StringBuffer = StringBuffer()
        for (b in digest) {
            //获取低八位有效值
            var i: Int = b.toInt() and 0xff
            //将整数转化为16进制
            var hexString = Integer.toHexString(i)
            if (hexString.length < 2) {
                //如果是一位的话，补0
                hexString = "0" + hexString
            }
            sb.append(hexString)
        }
        return sb.toString()

    } catch (e: NoSuchAlgorithmException) {
        e.printStackTrace()
    }

    return ""
}

//String轉日期
fun String.toDate(pattern: String? = "yyyy-MM-dd HH:mm:ss"): Date? {
    val sdFormat = try {
        SimpleDateFormat(pattern, Locale.ENGLISH)
    } catch (e: IllegalArgumentException) {
        null
    }
    val date = sdFormat?.let {
        try {
            it.parse(this)
        } catch (e: ParseException) {
            null
        }
    }
    return date
}

val YYYYMMDD_HHMMSS = "yyyy-MM-dd HH:mm:ss"
val YYYYMMDD = "yyyy-MM-dd"
val MMDD_HHMM = "MM-dd HH:mm"
val MMDD = "MM-dd"
val HHMM = "HH:mm"
fun formatDate(date: Date, pattern: String? = "yyyy-MM-dd HH:mm:ss"): String = SimpleDateFormat(pattern, Locale.ENGLISH).format(date)

//取得兩個時間的相差
fun timeLeave(targetDate: Date): Int {
    val calendar = Calendar.getInstance()
    var date1 = calendar.time
    var date2 = targetDate

    var ut1 = date1.time
    var ut2 = date2.time

    var timeP = ut1 - ut2
    var hr = timeP / (1000 * 60 * 60)

    var hrLeave = 72 - hr

    return if (hrLeave > 0) hrLeave.toInt() else 0
}

//Boolean 轉數字
val Boolean.int
    get() = if (this) 1 else 0
